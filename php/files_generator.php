<html>
    <head>
      <title>Gerador de Arquivos</title>
      <link rel="stylesheet" href="gerador_de_arquivos.css">
      <script src="../js/jquery-1.11.0.js"> </script>
      <script src ="../js/jConnect.js"> </script>   
    </head>
    
    <body>
      <div class="cabecalho">
        <h1> <img src="img/ptce.png" alt="Logotipo PTCE"> </h1>
      </div>
      
      <form method = "post" action = "files_generator_resp.php">

        <p id="titulo">Gerador de arquivos para o Presente!</p>

        <p>O f�rum a ser analisado pertence � qual curso/disciplina?<br></p>
         <select name="curso" id="cursos">
            <!-- Aqui sera preenchido com a busca MySQL !-->
         </select>

        <div id="selectForuns"> 
        <p>Quais f�runs voc� deseja analisar? <br>
          <select name="foruns" id="foruns">
            <option value="todos">TODOS</option>
            <!-- Demais opcoes serao carregadas da busca MySQL ! -->
          </select>
        </p>
        </div>

        <div id="botao">
          <input type="submit" value="Gerar arquivos" />
        </div>
      </form>
      
      <div class="rodape">
        <h1> <img src="img/nie.png" alt="Logotipo do NIE"> </h1>
      </div>

    </body>
</html>
